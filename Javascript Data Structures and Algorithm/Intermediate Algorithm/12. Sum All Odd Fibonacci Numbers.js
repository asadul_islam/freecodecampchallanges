function sumFibs(num) {
  let sum = 0;
  let a = 1;
  let b = 1;
  let tmp = 1;
  while( a <= num){
  	sum = (a % 2 == 1) ? sum + a : sum;
  	tmp = a;
  	a = b;
  	b += tmp;
  }
  return sum;
}

console.log(sumFibs(75025));