function orbitalPeriod(arr) {
  var GM = 398600.4418;
  var earthRadius = 6367.4447;

  return arr.map((element)=>{
    let a = Math.pow(earthRadius + element.avgAlt, 3);
    let b = Math.sqrt(a / GM);
    let p = Math.round(2 * Math.PI * b);
    delete element.avgAlt;
    element.orbitalPeriod = p;
    return element;
  });
}

console.log(orbitalPeriod([{name: "iss", avgAlt: 413.6}, {name: "hubble", avgAlt: 556.7}, {name: "moon", avgAlt: 378632.553}]));