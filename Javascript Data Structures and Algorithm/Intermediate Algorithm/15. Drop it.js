function dropElements(arr, func) {
  let i = 0;
  while(i < arr.length){
  	if(func(arr[0])){
  		break;
  	}
  	arr.shift();
  }
  return arr;
}

console.log(dropElements([0, 1, 0, 1], function(n) {return n === 1;}));