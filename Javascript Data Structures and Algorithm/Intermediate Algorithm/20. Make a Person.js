var Person = function(firstAndLast) {
	let names = firstAndLast.split(' ');
  this.setFirstName = (first)=>{
  	names[0] = first;
  };

  this.setLastName = (last)=>{
  	names[1] = last;
  };

  this.setFullName = (firstAndLast)=>{
  	names[0] = firstAndLast.split(' ')[0];
  	names[1] = firstAndLast.split(' ')[1];
  };

  this.getFirstName = ()=>{
    return names[0];
  };

  this.getLastName = ()=>{
    return names[1];
  };

  this.getFullName = ()=>{
  	return names[0] + " " + names[1];
  };
};

var bob = new Person('Bob Ross');
bob.setFirstName("Haskell");
console.log(bob.getFullName());
console.log(bob instanceof Person);
console.log(Object.keys(bob));
