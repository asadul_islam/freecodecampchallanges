function destroyer(arr, ...args) {
  return arr.concat(args).filter((element)=>{
  	if(args.indexOf(element) == -1){
  		return element;
  	}
  });
}

//destroyer([1, 2, 3, 1, 2, 3], 2, 3)

console.log(destroyer([1, 2, 3, 5, 1, 2, 3], 2, 3));