function fearNotLetter(str) {
  let arr  = []; 
  for(let i = str.charCodeAt(0); i <= str.charCodeAt(str.length - 1); i++){
  	if(!str.match(String.fromCharCode(i))){
  		arr.push(String.fromCharCode(i));
  	}
  }
  if(arr.length){
  	return arr.join('');
  }
  return undefined;
}

console.log(fearNotLetter("bcdf"));