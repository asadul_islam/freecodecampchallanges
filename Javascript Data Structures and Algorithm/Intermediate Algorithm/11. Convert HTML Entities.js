function convertHTML(str) {
  return str.split('').map((ch)=>{
  	if(ch.match(/&/)){
  		return '&amp;';
  	} else if(ch.match(/"/)){
  		return '&quot;';
  	}
  	else if(ch.match(/'/)){
  		return '&apos;';
  	}else if(ch.match(/</)){
  		return '&lt;';
  	}else if(ch.match(/>/)){
  		return '&gt;';
  	}
  	return ch;
  }).join('');
}

console.log(convertHTML("Schindler's List"));