function uniteUnique(...arr) {
  let result = [];
  let tmp = [];
  let p;
  tmp.push(...arr);
  for (let i = 0; i < tmp.length; i++) {
    result.push(...tmp[i]);
  }

  return result.filter((a) => {
    if (!tmp.some((b) => {
        return b === a;
      })) {
      tmp.push(a);
      return a;
    }
  });
}

console.log(uniteUnique([1, 2, 3], [5, 2, 1, 4], [2, 1], [6, 7, 8]));