function steamrollArray(arr) {
  let a = [];
  let stack = [];
  stack.push(...arr);
  while(stack.length > 0){
  	let b = stack.shift();
  	if(Array.isArray(b)){
  		stack.unshift(...b);
  	}
  	else {
  		a.push(b);
  	}
  }
  return a;
}

console.log(steamrollArray([1, {}, [3, [[4]]]]));