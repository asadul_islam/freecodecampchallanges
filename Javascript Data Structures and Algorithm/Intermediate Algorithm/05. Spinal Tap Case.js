function spinalCase(str) {
  return str.split('').map((element) => {
    if (element >= "A" && element <= "Z") {
      return " " + element;
    }
    return element;
  })
  .join('').split(/[^A-Za-z0-9]/).filter((element) => {
    if (element) {
      return element;
    }
  }).join('-').toLowerCase();
}

console.log(spinalCase('Teletubbies say Eh-oh'));