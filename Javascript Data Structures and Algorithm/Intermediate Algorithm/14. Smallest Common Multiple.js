function gcd(a, b){
	if(b === 0){
		return a;
	}
	return gcd(b, a % b);
}

function smallestCommons(arr) {
  let sum = 1;
  for(let i = Math.min(...arr); i <= Math.max(...arr); i++){
  	sum = (i * sum) / gcd(i, sum);
  }
  return sum;
}

console.log(smallestCommons([2,10]));