function myReplace(str, before, after) {
  if(before[0].match(/[A-Z]/)){
  	return str.replace(before, after.replace(after[0], after[0].toUpperCase()));
  }
  return str.replace(before, after.replace(after[0], after[0].toLowerCase()));
}

console.log(myReplace("He is Sleeping on the couch", "Sleeping", "sitting"));