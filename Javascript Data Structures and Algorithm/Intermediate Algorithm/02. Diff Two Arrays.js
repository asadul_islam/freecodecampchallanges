function diffArray(arr1, arr2) {
  var newArr = arr1.concat(arr2).filter((element) => {
    if (arr1.indexOf(element) == -1 || arr2.indexOf(element) == -1) {
      return element;
    }
  });

  return newArr;
}

console.log(diffArray([1, 2, 3, 5], [1, 2, 3, 4, 5]));