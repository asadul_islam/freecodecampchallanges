function addTogether(...arr) {
  if (!Number.isInteger(arr[0]) || (arr.length === 2 && !Number.isInteger(arr[1]))) {
    return undefined;
  }
  if (arr.length === 2 && Number.isInteger(arr[0]) && Number.isInteger(arr[1])) {
    return arr[0] + arr[1];
  }

  return function (b) {
  	if(!Number.isInteger(b)){
  		return undefined;
  	}
    return arr[0] + b;
  }
}

let result = addTogether(2)([3]) ;
console.log(result); 
