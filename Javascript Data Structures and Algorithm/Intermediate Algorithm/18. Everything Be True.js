function truthCheck(collection, pre) {
  for(let i = 0; i < collection.length; i++){
  	if(!collection[i][pre]){
  		return false;
  	}
  }
  return true;
}

let result = truthCheck([{"user": "Tinky-Winky", "sex": "male"}, {"user": "Dipsy"}, {"user": "Laa-Laa", "sex": "female"}, {"user": "Po", "sex": "female"}], "sex");
console.log(result);