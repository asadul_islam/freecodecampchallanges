function translatePigLatin(str) {
  if(str[0].match(/[aeiou]/i)){
    return str + "way";
  }

  if(!str.match(/[aeiou]/gi)){
    return str + "ay";
  }

  let arr = [];
  for(let i = 0; i < str.length; i++){
    if(str[i].match(/[aeiou]/i)){
      return str.substring(i) + arr.join('') + "ay";
    }
    arr.push(str[i]);
  }
}

console.log(translatePigLatin("asddd"));