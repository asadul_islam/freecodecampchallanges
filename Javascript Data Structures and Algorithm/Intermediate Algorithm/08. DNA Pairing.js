let obj =  {
  "A": "T",
  "T": "A",
  "C": "G",
  "G": "C"
};

function pairElement(str) {
  let arr = [];
  for(let i = 0; i < str.length; i++){
    arr.push([str[i], obj[str[i]]]);
  }
  return arr;
}

console.log(pairElement("ATCGA"));