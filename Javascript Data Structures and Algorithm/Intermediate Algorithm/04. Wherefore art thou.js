function whatIsInAName(collection, source) {
  var arr = collection.filter((element)=>{
  	for(let key of Object.keys(source)){
  		if(!element.hasOwnProperty(key) || element[key] != source[key]){
  			return;
  		}
  	}
  	return element;
  });
  return arr;
}

console.log(whatIsInAName([{ "apple": 1, "bat": 2 }, { "bat": 2 }, { "apple": 1, "bat": 2, "cookie": 2 }], { "apple": 1, "bat": 2 }));