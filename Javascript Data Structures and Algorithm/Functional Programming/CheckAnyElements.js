function checkPositive(arr) {
  // Add your code below this line
  return arr.some((a)=>{
  	return a >= 0;
  }); 
  
  // Add your code above this line
}
let r = checkPositive([-2, -4]);
console.log(r);