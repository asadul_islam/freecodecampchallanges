function rot13(str) { 
  return str.split('').map((element)=>{
  	let letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  	if(element >= 'A' && element <= 'Z'){
  		return  letters[(letters.indexOf(element) - 13 + 26) % 26];
  	}
  	else{
  		return element;
  	}
  }).join('');
}

// Change the inputs below to test
console.log(rot13("GUR DHVPX OEBJA SBK WHZCF BIRE GUR YNML QBT."));