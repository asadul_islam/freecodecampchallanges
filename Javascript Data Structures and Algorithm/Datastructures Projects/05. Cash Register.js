function calculateTotalCash(cid) {
  let sum = 0;
  for (let c of cid) {
    sum += c[1];
  }
  return sum;
}

function checkCashRegister(price, cash, cid) {
  var value = {
    'ONE HUNDRED': 100.00,
    'TWENTY': 20.00,
    'TEN': 10.00,
    'FIVE': 5.00,
    'ONE': 1.00,
    'QUARTER': 0.25,
    'DIME': 0.10,
    'NICKEL': 0.05,
    'PENNY': 0.01
  }
  var diff = cash - price;
  var change = [];
  var total = calculateTotalCash(cid);

  if (total < diff) {
    return { status: "INSUFFICIENT_FUNDS", change: [] };
  }
  if (total === diff) {
    return { status: "CLOSED", change: cid };
  }

  for (let i = cid.length - 1; i >= 0; i--) {
    let changeValue = 0;
    if (diff >= value[cid[i][0]]) {
      while ((diff - value[cid[i][0]]) >= 0 && value[cid[i][0]] <= cid[i][1]) {
        diff -= value[cid[i][0]];
        cid[i][1] -= value[cid[i][0]];
        changeValue += value[cid[i][0]];
        diff = Math.round(diff * 100) / 100;
        //console.log(diff + " " + value[cid[i][0]]);
      }
      if (changeValue > 0) {
        change.push([cid[i][0], changeValue]);
      }
      //console.log(diff + " " + value[cid[i][0]] + " " + cid[i][1] + " " + change);
    }
  }
  if (change.length <= 0 || diff) {
    return { status: "INSUFFICIENT_FUNDS", change: [] };
  }
  return { status: "OPEN", change: change };
}

console.log(checkCashRegister(19.5, 20, [["PENNY", 1.01], ["NICKEL", 2.05], ["DIME", 3.1], ["QUARTER", 4.25], ["ONE", 90], ["FIVE", 55], ["TEN", 20], ["TWENTY", 60], ["ONE HUNDRED", 100]]));