function palindrome(str) {
  let st = str.toLowerCase().split('').filter((ch)=>{
  	if(ch.match(/[a-z0-9]/i)){
  		return ch;
  	}
  });

  while(st.length > 1){
  	let a = st.shift();
  	let b = st.pop();
  	if(a != b){
  		return false;
  	}
  }
  return true;
}

console.log(palindrome("five|\_/|four"));