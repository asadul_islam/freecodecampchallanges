function telephoneCheck(str) {
  if (str.match(/^(1\s?)?(\(\d{3}\)|\d{3})[\s\-]?\d{3}[\s\-]?\d{4}$/)) {
    return true;
  }

  return false;
}

console.log(telephoneCheck("2 7572 622 7382"));