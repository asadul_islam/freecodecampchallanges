function convertToRoman(num) {
  let C = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"];
  let X = ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"];
  let I = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"];
  let k = Math.floor(num / 1000);
  num = num % 1000;
  let r = "";
  for(let j = 0; j < k; j++){
  	r += 'M';
  }
  return r += C[Math.floor(num / 100)] + X[Math.floor((num % 100) / 10)] + I[num % 10];
}

console.log(convertToRoman(2014));